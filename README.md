# Proyecto final

## Objetivo

```
El consorcio NOVAMEDIK en su “Proyecto de Ampliación Tecnológica 2019-2021” busca implementar distintos tipos de soluciones tecnológicas que pretenden mejorar la calidad del servicio y a su vez la calidad de la atención médica a los pacientes. Los tres ejes que se buscan mejorar son: atención médica, logística, y organización.
```

El proyecto se desarrollará utilizando HTML, CSS (puedes usar un framework para los grids), Javascript (puedes usar frameworks) y cualquier tecnología escogida por tí para la parte de back-end.

## Especificaciones

* __Entorno__: [NodeJS](https://nodejs.org/) + [Express](https://expressjs.com/)
* __Base de datos__: [MySQL](https://mysql.com/)
* __Variables de entorno__: [Dotenv](https://www.npmjs.com/package/dotenv)
* __Credenciales__: [JsonWebToken](https://jwt.io/)
* __PDF__: [pdfjs](https://www.npmjs.com/package/pdfjs)

## Requisitos

* __Sistema Operativo__: No es necesario un sistema operativo en específico.
* __RAM__: 1GB (como mínimo)
* __Procesador__: Intel i3 o AMD Ryzen 3 (como mínimo)
* __Base de datos__: una instancia de MySQL

## Instrucciones

Se debe tener previamente una instancia de MySQL corriendo o acceso a ésta.

1. __Especificar las variables de entorno__

En entorno de desarrollo se necesita copiar el archivo ```.env.example``` con el nombre ```.env``` y usar las credenciales propias.

> cp .env.example .env

En producción 

1. __Iniciar__

Para iniciar el proyecto en desarrollo se ejecuta el comando:

> npm run dev

Para iniciar el proyecto en producción se ejecuta el comando:

> npm start
