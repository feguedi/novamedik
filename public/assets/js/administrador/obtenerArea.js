const areaNombre = decodeURI(getUrlParams()['area'])

document.addEventListener('DOMContentLoaded', function () {
    if (!areaNombre) {
        window.location = '/habitaciones'
    }
    $.ajax({
        method: 'GET',
        url: '/api/area/'.concat(areaNombre),
        headers: {
            'Authorization': 'Bearer '.concat(token),
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        beforeSend: function (xhr) {
            $('#row-equipo-medico, #row-habitaciones').html(`<div class="col-12" id="col-cargando">${cargando}</div>`)
        }
    }).done(function (respuesta) {
        $('.row > #col-cargando').remove()
        $('.breadcrumb > .breadcrumb-item:nth-child(3) > a > h4').text(areaNombre)
        console.log('datos:', respuesta)
        $('#row-equipo-medico').append(cardsEquipoMedico(respuesta.area))
        $('#row-equipo-medico').append(botonAsignar)
        $('#row-habitaciones').append(cardsHabitaciones(respuesta.area))
    }).fail(function (error) {
        $('.row > #col-cargando').remove()
    })
})

function cardsEquipoMedico (habitaciones) {
    const todoPersonal = []
    Object.keys(habitaciones).forEach(function (habitacion) {
        todoPersonal.push(...habitaciones[habitacion].equipoMedico)
    })
    // https://stackoverflow.com/questions/2218999/remove-duplicates-from-an-array-of-objects-in-javascript/36744732
    const eqMed = todoPersonal.filter(function (personal, index, self) {
        return index === self.findIndex(function (obj) {
            return obj.id === personal.id && obj.turno === personal.turno
        })
    })
    return eqMed.map(function (personal) {
        return `<div class="col-6 col-md-3 my-2">
            <a href="/usuario?usuario=${personal.id}" class="card h-100 card-paciente shadow-sm text-dark" style="text-decoration: none;">
                <div class="card-body">
                    <h5 class="text-center card-title card-nombres">${personal.nombres}</h5>
                    <h6 class="text-center card-subtitle card-apellidos">${personal.apellidos}</h6>
                </div>
                <div class="card-footer text-center card-footer-historial">
                    <p class="text-muted">Turno: ${personal.horario}</p>
                </div>
            </a>
        </div>`
    })
}

function cardsHabitaciones (habitaciones) {
    return Object.keys(habitaciones).map(function (habitacion) {
        const paciente = habitaciones[habitacion].paciente
        return `<div class="col-6 col-md-2 my-2">
            <div class="card h-100 card-paciente shadow-sm ${paciente ? 'card-ocupado' : ''}">
                <div class="card-body">
                    <h5 class="text-center card-title">${habitacion}</h5>
                    ${paciente ? `<h6 class="text-center card-title card-nombres">${paciente.nombres}</h6>
                    <h6 class="text-center card-subtitle card-apellidos">${paciente.apellidos}</h6>` : ''}
                </div>
                <div class="card-footer text-center card-footer-historial">
                    <p>Piso: ${habitaciones[habitacion].piso}</p>
                </div>
            </div>
        </div>`
    })
}

const botonAsignar = `<a href="/asignar/habitaciones.html?area=${areaNombre}" class="col-6 col-md-3 my-2" style="text-decoration: none;">
    <div id="card-nuevo-personal" class="card-body d-flex flex-column justify-content-center h-100 rounded shadow-sm text-black-50">
        <div class="text-center icon-nuevo-personal">
            <i class="fa fa-plus-circle fa-3x"></i>
        </div>
        <h4 class="text-uppercase text-center card-title m-0">Asignar personal</h4>
    </div>
</a>`
